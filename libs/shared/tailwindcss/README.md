# shared-tailwindcss

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test shared-tailwindcss` to execute the unit tests via [Jest](https://jestjs.io).
