import { expect } from 'expect';
import postcss from 'postcss';
import tailwindcss, { Config } from 'tailwindcss';

export const runWithConfig = async (config: Config) => {
  const { currentTestName, testPath } = expect.getState();
  if (!currentTestName || !testPath) {
    throw new Error(
      'TailwindCSS tests must be run with Jest and the `testName` and `testPath` globals set.',
    );
  }
  const path = `${testPath}?test=${Buffer.from(currentTestName).toString(
    'base64',
  )}`;

  config = {
    corePlugins: { preflight: false },
    ...config,
  };

  const result = await postcss(
    tailwindcss({
      ...config,
    }),
  ).process('@tailwind utilities; @tailwind components;', {
    from: path,
    to: path,
  });
  return result.css;
};
