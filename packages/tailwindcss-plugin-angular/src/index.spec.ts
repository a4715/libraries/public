import { runWithConfig } from '@libs/shared/tailwindcss/tests/utils/runWithConfig';

import tailwindcssPluginAngular from './index';

describe('plugin', () => {
  it('should work', async () => {
    // GIVEN
    const config = {
      content: [
        {
          raw: `
            <a href="#" class="text-white active-link:text-black" />
            `,
        },
      ],
      plugins: [tailwindcssPluginAngular],
    };

    // WHEN
    const result = await runWithConfig(config);

    // THEN
    expect(result).toMatchSnapshot();
  });

  it('should work with custom config', async () => {
    // GIVEN
    const config = {
      content: [
        {
          raw: `
            <a href="#" class="text-white custom-active-class:text-black" />
            `,
        },
      ],
      angular: {
        router: {
          activeClass: 'custom-active-class',
        },
      },
      plugins: [tailwindcssPluginAngular],
    };

    // WHEN
    const result = await runWithConfig(config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
