import plugin from 'tailwindcss/plugin';
import { routerVariants } from './lib/router/routerVariants';

export default plugin(({ addVariant, config }) => {
  routerVariants({ addVariant, config });
});
