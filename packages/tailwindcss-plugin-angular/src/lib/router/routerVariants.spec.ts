import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import plugin from 'tailwindcss/plugin';
import { routerVariants } from './routerVariants';

describe('routerVariants', () => {
  it('should work with default class', async () => {
    // GIVEN
    const routerVariantsPlugin = plugin(({ addVariant, config }) => {
      routerVariants({ addVariant, config });
    });
    const config = {
      content: [
        {
          raw: `
            <a href="#" class="text-white active-link:text-black" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(routerVariantsPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });

  it('should work with custom defined class', async () => {
    // GIVEN
    const routerVariantsPlugin = plugin(({ addVariant, config }) => {
      routerVariants({
        addVariant,
        config,
      });
    });
    const config = {
      content: [
        {
          raw: `
            <a href="#" class="text-white custom-active-class:text-black" />
            `,
        },
      ],
      angular: {
        router: {
          activeClass: 'custom-active-class',
        },
      },
    };

    // WHEN
    const result = await run(routerVariantsPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
