import { PluginAPI } from 'tailwindcss/types/config';

export const routerVariants = ({
  addVariant,
  config,
}: {
  addVariant: PluginAPI['addVariant'];
  config: PluginAPI['config'];
}) => {
  const routerConfig = config<RouterVariantsConfig>('angular.router', {
    activeClass: 'active-link',
  });
  addVariant(routerConfig.activeClass, `.${routerConfig.activeClass}&`);
};
