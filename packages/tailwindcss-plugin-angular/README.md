# tailwindcss-plugin-angular

This is a plugin for [Tailwind CSS](https://tailwindcss.com/) that adds support for
the [Angular framework](https://angular.io).
Using it enables you to easily style Angular's components and directives using Tailwind CSS classes.

```bash
npm install --save-dev @endorian/tailwindcss-plugin-angular
```

**Note: This plugin requires Tailwind CSS as peer dependency.**

## ⚙️ Configuration

Add the plugin to your `tailwind.config.js` file:

**Using CommonJS**

```js
// tailwind.config.js
module.exports = {
  // ...
  plugins: [require('@endorian/tailwindcss-plugin-angular')]
}
```

**Using ES Modules**

```js
// tailwind.config.js
import tailwindcssPluginAngular from '@endorian/tailwindcss-plugin-angular'

module.exports = {
  // ...
  plugins: [tailwindcssPluginAngular]
}
```

### Custom plugin configuration

If you want to use a custom configuration, you must use `angular` als key in your `tailwind.config.js` file:

```js
// tailwind.config.js
module.exports = {
  // ...
  angular: {
    // ...
  }
}
```

## 🛠️ Usage

### Router Link

```html
<!-- Example -->
<a
  routerLink="/home"
  routerLinkActive="active-link"
  class="text-red-300 active-link:text-red-500">
  Home
</a>
```

As in the example the default active class is `active-link`, like
in [Angular's documentation](https://angular.io/api/router/RouterLinkActive). You can change this by
setting the config:

```js
// tailwind.config.js
module.exports = {
  // ...
  angular: {
    // ...
    router: {
      activeClass: 'my-active-class'
    }
  }
}
```

and then use it like this:

```html
<a
  routerLink="/home"
  routerLinkActive="my-active-class"
  class="text-red-300 my-active-class:text-red-500">
  Home
</a>
```

## 📄 License and Contribution

This plugin is licensed under the MIT License. Feel free to contribute by creating a merge request or by opening an
issue if you find a bug or want to request a feature. 
