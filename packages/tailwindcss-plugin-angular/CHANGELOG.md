## [1.0.1](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-angular-v1.0.0...tailwindcss-plugin-angular-v1.0.1) (2023-10-13)


### Bug Fixes

* correct output path ([0511e50](https://gitlab.com/a4715/libraries/public/commit/0511e50c3b3324f2266b14f78db3b1f96ace8da0))

# 1.0.0 (2023-10-13)


### Bug Fixes

* add plugin test ([e9b404d](https://gitlab.com/a4715/libraries/public/commit/e9b404d095ce8ae804b8bb0e061c03eee80955a6))


### Features

* kick-start plugin with router variants ([53b0bda](https://gitlab.com/a4715/libraries/public/commit/53b0bda259d50e26168cc8beec8c5069164c49be))

# 1.0.0 (2023-10-13)


### Features

* kick-start plugin with router variants ([53b0bda](https://gitlab.com/a4715/libraries/public/commit/53b0bda259d50e26168cc8beec8c5069164c49be))
