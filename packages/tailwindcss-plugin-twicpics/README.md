# tailwindcss-plugin-twicpics

A Tailwind CSS plugin to use
TwicPics's [Style-Driven Approach](https://www.twicpics.com/docs/components/next#style-driven-approach).

```bash
npm install --save-dev @endorian/tailwindcss-plugin-css-twicpics
```

**Note: This plugin requires Tailwind CSS as peer dependency.**

## ⚙️ Configuration

Add the plugin to your `tailwind.config.js` file:

**Using CommonJS**

```js
// tailwind.config.js
module.exports = {
  // ...
  plugins: [require('@endorian/tailwindcss-plugin-twicpics')]
}
```

**Using ES Modules**

```js
// tailwind.config.js
import tailwindcssPluginTwicPics from '@endorian/tailwindcss-plugin-twicpics'

module.exports = {
  // ...
  plugins: [tailwindcssPluginTwicPics]
}
```

## 🛠️ Usage

This plugin adds multiple utility classes to your Tailwind CSS configuration to use
TwicPics's [Style-Driven Approach](https://www.twicpics.com/docs/components/next#style-driven-approach). These utility
classes will add CSS variables to the element containing the class.

### Mode

`.twic-mode-{mode}`

This utility allows you to set the mode.

Possible modes from [TwicPics' documentation](https://www.twicpics.com/docs/components/next#css-variables) are:

* `cover`
* `contain`

```html
<!-- Example -->
<div class="twic-mode-contain">
  ...
</div>
```

### Position

`.twic-position-{position}`

This utility allows you to set the position.

Possible positions from [TwicPics' documentation](https://www.twicpics.com/docs/components/next#css-variables) are:

* `top`
* `right`
* `bottom`
* `left`
* `center`
* `top-left`
* `top-right`
* `bottom-left`
* `bottom-right`

```html
<!-- Example -->
<div class="twic-position-top-right">
  ...
</div>
```

### Ratio

`.twic-ratio-{ratio}`

This utility allows you to set the ratio.

Predefined values are:

* `square` (1)
* `video` (16/9)
* `landscape` (4/3)
* `portrait` (2/3)

```html
<!-- Example -->
<div class="twic-ratio-square">
  ...
</div>
```

You can add your ratios by extending your Tailwind CSS configuration like in the following example:

```js
// tailwind.config.js
module.exports = {
  theme: {
    // ...
    extend: {
      // ...
      twicRatio: {
        'myratio': '1/2'
      }
    }
  }
}
```

### Transition delay

`.twic-delay-{delay}`

This utility allows you to set the delay. Per default, you can
use [Tailwind's transition delays](https://tailwindcss.com/docs/transition-delay).

```html
<!-- Example -->
<div class="twic-delay-150">
  ...
</div>
```

You can add your TwicPics-related delays by extending your Tailwind CSS configuration like in the following example:

```js
// tailwind.config.js
module.exports = {
  theme: {
    // ...
    extend: {
      // ...
      twicTransitionDelay: {
        'mydelay': '150ms'
      }
    }
  }
}
```

### Transition duration

`.twic-duration-{duration}`

This utility allows you to set the transition duration. Per default, you can
use [Tailwind's transition durations](https://tailwindcss.com/docs/transition-duration).

```html
<!-- Example -->
<div class="twic-duration-300">
  ...
</div>
```

When you miss some durations, you can add your TwicPics-related durations by extending your Tailwind CSS configuration
like in the following example:

```js
// tailwind.config.js
module.exports = {
  theme: {
    // ...
    extend: {
      // ...
      twicTransitionDuration: {
        'myDuration': '300ms'
      }
    }
  }
}
```

### Transition timing functions

`.twic-{easing}`

This utility allows you to set the transition timing function. Per default, you can use [Tailwind's transition timing
functions](https://tailwindcss.com/docs/transition-timing-function).

```html
<!-- Example -->
<div class="twic-in-out">
  ...
</div>
```

You can use your timing functions by extending your Tailwind CSS configuration, like in the following example:

```js
// tailwind.config.js
module.exports = {
  theme: {
    // ...
    extend: {
      // ...
      twicTransitionTimingFunction: {
        'myTimingFunction': 'cubic-bezier(...)'
      }
    }
  }
}
```

### Zoom

`.twic-zoom-{zoom}`

This utility allows you to set the zoom.

Predefined zooms are:

* `1`

```html
<!-- Example -->
<div class="twic-zoom-1">
  ...
</div>
```

When you miss some zooms, you can add your TwicPics-related zooms by extending your Tailwind CSS configuration like in
the following example:

```js
// tailwind.config.js
module.exports = {
  theme: {
    // ...
    extend: {
      // ...
      twicZoom: {
        'myZoom': '2'
      }
    }
  }
}
```

## 📄 License and Contribution

This plugin is licensed under the MIT License. Feel free to contribute by creating a merge request or by opening an
issue if you find a bug or want to request a feature. 
