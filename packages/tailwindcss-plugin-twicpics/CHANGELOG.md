# [0.1.0](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.8...tailwindcss-plugin-twicpics-v0.1.0) (2023-10-13)


### Features

* kick-start plugin with router variants ([53b0bda](https://gitlab.com/a4715/libraries/public/commit/53b0bda259d50e26168cc8beec8c5069164c49be))

## [0.0.8](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.7...tailwindcss-plugin-twicpics-v0.0.8) (2023-10-13)


### Bug Fixes

* **docs:** correct installation command in README.md ([0906a08](https://gitlab.com/a4715/libraries/public/commit/0906a08ba439d3c7a8c8c343e46ed40d2d5c0c2a))

## [0.0.7](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.6...tailwindcss-plugin-twicpics-v0.0.7) (2023-10-12)

### Bug Fixes

* add missing homepage property and update
  README.md ([48f032c](https://gitlab.com/a4715/libraries/public/commit/48f032c654a81281a8dce95924b00325d1b73d22))
* correct
  dependencies ([dc418d5](https://gitlab.com/a4715/libraries/public/commit/dc418d55b18467fd46717e860e5e29c5b63c5a54))

## [0.0.6](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.5...tailwindcss-plugin-twicpics-v0.0.6) (2023-10-12)

### Bug Fixes

* npm publish ([40623ac](https://gitlab.com/a4715/libraries/public/commit/40623acb96b8ead0f2f4b84e93f786c27415526f))
* npm publish ([197d6d9](https://gitlab.com/a4715/libraries/public/commit/197d6d91ca513c71132a070735610c3ac0878846))

## [0.0.5](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.4...tailwindcss-plugin-twicpics-v0.0.5) (2023-10-12)

### Bug Fixes

* npm publish ([390f38e](https://gitlab.com/a4715/libraries/public/commit/390f38ecfa56694500610cddd2d881958d159546))
* npm publish ([fda6946](https://gitlab.com/a4715/libraries/public/commit/fda694627c3bdb9633c6fb12cee83c5a72fccff1))

## [0.0.4](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.3...tailwindcss-plugin-twicpics-v0.0.4) (2023-10-12)

### Bug Fixes

* npm publish ([09a02ba](https://gitlab.com/a4715/libraries/public/commit/09a02baa1a3538305dec2e6313b03d4499a85fdc))
* npm publish ([8bfae49](https://gitlab.com/a4715/libraries/public/commit/8bfae4926ebb67b64920160511c599fe59bd3f5f))

## [0.0.4](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.3...tailwindcss-plugin-twicpics-v0.0.4) (2023-10-12)

### Bug Fixes

* npm publish ([8bfae49](https://gitlab.com/a4715/libraries/public/commit/8bfae4926ebb67b64920160511c599fe59bd3f5f))

## [0.0.3](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.2...tailwindcss-plugin-twicpics-v0.0.3) (2023-10-12)

### Bug Fixes

* test ([edcf01b](https://gitlab.com/a4715/libraries/public/commit/edcf01bbfb06943a2deb9b31dced0402bbb312ec))

## [0.0.2](https://gitlab.com/a4715/libraries/public/compare/tailwindcss-plugin-twicpics-v0.0.1...tailwindcss-plugin-twicpics-v0.0.2) (2023-10-12)

### Bug Fixes

* test ([35038cb](https://gitlab.com/a4715/libraries/public/commit/35038cbd0494537013e03c22263bbb0cc0741a95))
* test ([0913406](https://gitlab.com/a4715/libraries/public/commit/09134062e5838a3eff298b0cedd13cfff1c4ffb0))
