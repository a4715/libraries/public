import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import { Config } from 'tailwindcss';
import plugin from 'tailwindcss/plugin';
import { transitionTimingFunctionUtilities } from './transitionTimingFunctionUtilities';

describe('transitionTimingFunctionUtilities', () => {
  const transitionTimingFunctionUtilitiesPlugin = plugin(
    ({ e, theme, addUtilities }) => {
      addUtilities([...transitionTimingFunctionUtilities({ e, theme })]);
    },
  );

  it('should work with predefined tailwind transitionTimingFunctions values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-in" />
            <div class="twic-out" />
            <div class="twic-in-out" />
            <div class="twic-linear" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(transitionTimingFunctionUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });

  it('should work with custom values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-foo" />
            `,
        },
      ],
      theme: {
        twicTransitionTimingFunction: {
          foo: 'bar',
        },
      },
    };

    // WHEN
    const result = await run(transitionTimingFunctionUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
