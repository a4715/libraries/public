import { PluginAPI } from 'tailwindcss/types/config';
import { utilitiesWithMergedPaths } from '../helpers/utilitiesWithMergedPaths';

export const transitionDelayUtilities = ({
  e,
  theme,
}: {
  e: PluginAPI['e'];
  theme: PluginAPI['theme'];
}) =>
  utilitiesWithMergedPaths({
    e,
    theme,
    tailwindConfigPath: 'transitionDelay',
    twicConfigPath: 'twicTransitionDelay',
    cssClassPrefix: 'twic-delay',
    cssVariableName: '--twic-transition-delay',
  });
