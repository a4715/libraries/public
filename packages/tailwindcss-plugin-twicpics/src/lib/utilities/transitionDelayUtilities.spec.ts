import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import { Config } from 'tailwindcss';
import plugin from 'tailwindcss/plugin';
import { transitionDelayUtilities } from './transitionDelayUtilities';

describe('transitionDelayUtilities', () => {
  const transitionDelayUtilitiesPlugin = plugin(
    ({ e, theme, addUtilities }) => {
      addUtilities([...transitionDelayUtilities({ e, theme })]);
    },
  );

  it('should work with predefined tailwind transitionDelay values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-delay-0" />
            <div class="twic-delay-75" />
            <div class="twic-delay-150" />
            <div class="twic-delay-300" />
            <div class="twic-delay-500" />
            <div class="twic-delay-700" />
            <div class="twic-delay-1000" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(transitionDelayUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });

  it('should work with custom values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-delay-2" />
            `,
        },
      ],
      theme: {
        twicTransitionDelay: {
          2: '2ms',
        },
      },
    };

    // WHEN
    const result = await run(transitionDelayUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
