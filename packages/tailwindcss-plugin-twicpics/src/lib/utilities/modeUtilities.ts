import { PluginAPI } from 'tailwindcss/types/config';
import { utilitiesWithAllowedKeysOnly } from '../helpers/utilitiesWithAllowedKeysOnly';

const ALLOWED_MODES = ['cover', 'contain'];

export const modeUtilities = ({ e }: { e: PluginAPI['e'] }) =>
  utilitiesWithAllowedKeysOnly({
    e,
    allowedKeys: ALLOWED_MODES,
    cssClassPrefix: 'twic-mode',
    cssVariableName: '--twic-mode',
  });
