import { PluginAPI } from 'tailwindcss/types/config';
import { utilitiesWithMergedPaths } from '../helpers/utilitiesWithMergedPaths';

export const transitionDurationUtilities = ({
  e,
  theme,
}: {
  e: PluginAPI['e'];
  theme: PluginAPI['theme'];
}) =>
  utilitiesWithMergedPaths({
    e,
    theme,
    tailwindConfigPath: 'transitionDuration',
    twicConfigPath: 'twicTransitionDuration',
    cssClassPrefix: 'twic-duration',
    cssVariableName: '--twic-transition-duration',
  });
