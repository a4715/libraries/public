import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import plugin from 'tailwindcss/plugin';
import { positionUtilities } from './positionUtilities';

describe('positionUtilities', () => {
  const positionUtilitiesPlugin = plugin(({ e, addUtilities }) => {
    addUtilities([...positionUtilities({ e })]);
  });

  it('should work with predefined values', async () => {
    // GIVEN
    const config = {
      content: [
        {
          raw: `
            <div class="twic-position-top" />
            <div class="twic-position-right" />
            <div class="twic-position-bottom" />
            <div class="twic-position-left" />
            <div class="twic-position-center" />
            <div class="twic-position-top-left" />
            <div class="twic-position-top-right" />
            <div class="twic-position-bottom-left" />
            <div class="twic-position-bottom-right" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(positionUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
