import { PluginAPI } from 'tailwindcss/types/config';
import { utilitiesWithMergedPaths } from '../helpers/utilitiesWithMergedPaths';

export const transitionTimingFunctionUtilities = ({
  e,
  theme,
}: {
  e: PluginAPI['e'];
  theme: PluginAPI['theme'];
}) =>
  utilitiesWithMergedPaths({
    e,
    theme,
    tailwindConfigPath: 'transitionTimingFunction',
    twicConfigPath: 'twicTransitionTimingFunction',
    cssClassPrefix: 'twic',
    cssVariableName: '--twic-transition-timing-function',
  });
