import { PluginAPI } from 'tailwindcss/types/config';
import { utilitiesWithDefaults } from '../helpers/utilitiesWithDefaults';

const RATIO_DEFAULTS = {
  video: '16/9',
  landscape: '4/3',
  portrait: '2/3',
  square: '1',
};

export const ratioUtilities = ({
  e,
  theme,
}: {
  e: PluginAPI['e'];
  theme: PluginAPI['theme'];
}) =>
  utilitiesWithDefaults({
    e,
    theme,
    defaults: RATIO_DEFAULTS,
    twicConfigPath: 'twicRatio',
    cssClassPrefix: 'twic-ratio',
    cssVariableName: '--twic-ratio',
    valueTransformer: (value) => `calc(${value})`,
  });
