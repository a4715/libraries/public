import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import { Config } from 'tailwindcss';
import plugin from 'tailwindcss/plugin';
import { zoomUtilities } from './zoomUtilities';

describe('zoomUtilities', () => {
  const zoomUtilitiesPlugin = plugin(({ e, theme, addUtilities }) => {
    addUtilities([...zoomUtilities({ e, theme })]);
  });

  it('should work with predefined values', async () => {
    // GIVEN
    const config = {
      content: [
        {
          raw: `
            <div class="twic-zoom-1" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(zoomUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });

  it('should work with custom values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-zoom-custom" />
            `,
        },
      ],
      theme: {
        twicZoom: {
          custom: '2',
        },
      },
    };
    // WHEN
    const result = await run(zoomUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
