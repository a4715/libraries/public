import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import { Config } from 'tailwindcss';
import plugin from 'tailwindcss/plugin';
import { transitionDurationUtilities } from './transitionDurationUtilities';

describe('transitionDurationUtilities', () => {
  const transitionDurationUtilitiesPlugin = plugin(
    ({ e, theme, addUtilities }) => {
      addUtilities([...transitionDurationUtilities({ e, theme })]);
    },
  );

  it('should work with predefined tailwind transitionDuration values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-duration-0" />
            <div class="twic-duration-75" />
            <div class="twic-duration-150" />
            <div class="twic-duration-300" />
            <div class="twic-duration-500" />
            <div class="twic-duration-700" />
            <div class="twic-duration-1000" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(transitionDurationUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });

  it('should work with custom values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-duration-149" />
            `,
        },
      ],
      theme: {
        twicTransitionDuration: {
          149: '149ms',
        },
      },
    };

    // WHEN
    const result = await run(transitionDurationUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
