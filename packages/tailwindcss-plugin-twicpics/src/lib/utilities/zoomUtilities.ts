import { PluginAPI } from 'tailwindcss/types/config';
import { utilitiesWithDefaults } from '../helpers/utilitiesWithDefaults';

const ZOOM_DEFAULTS = {
  1: '1',
};

export const zoomUtilities = ({
  e,
  theme,
}: {
  e: PluginAPI['e'];
  theme: PluginAPI['theme'];
}) =>
  utilitiesWithDefaults({
    e,
    theme,
    defaults: ZOOM_DEFAULTS,
    twicConfigPath: 'twicZoom',
    cssClassPrefix: 'twic-zoom',
    cssVariableName: '--twic-zoom',
  });
