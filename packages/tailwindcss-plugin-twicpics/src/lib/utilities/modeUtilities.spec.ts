import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import plugin from 'tailwindcss/plugin';
import { modeUtilities } from './modeUtilities';

describe('modeUtilities', () => {
  const modeUtilitiesPlugin = plugin(({ e, addUtilities }) => {
    addUtilities([...modeUtilities({ e })]);
  });

  it('should work with predefined values', async () => {
    // GIVEN
    const config = {
      content: [
        {
          raw: `
            <div class="twic-mode-contain" />
            <div class="twic-mode-cover" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(modeUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
