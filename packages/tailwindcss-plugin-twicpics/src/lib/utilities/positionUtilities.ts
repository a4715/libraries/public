import { PluginAPI } from 'tailwindcss/types/config';
import { utilitiesWithAllowedKeysOnly } from '../helpers/utilitiesWithAllowedKeysOnly';

const ALLOWED_POSITIONS = [
  'center',
  'top',
  'right',
  'bottom',
  'left',
  'top-left',
  'top-right',
  'bottom-left',
  'bottom-right',
];

export const positionUtilities = ({ e }: { e: PluginAPI['e'] }) =>
  utilitiesWithAllowedKeysOnly({
    e,
    allowedKeys: ALLOWED_POSITIONS,
    cssClassPrefix: 'twic-position',
    cssVariableName: '--twic-position',
  });
