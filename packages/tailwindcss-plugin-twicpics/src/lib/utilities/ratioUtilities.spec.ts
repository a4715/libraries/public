import { run } from '@libs/shared/tailwindcss/tests/utils/run';
import { Config } from 'tailwindcss';
import plugin from 'tailwindcss/plugin';
import { ratioUtilities } from './ratioUtilities';

describe('ratioUtilities', () => {
  const ratioUtilitiesPlugin = plugin(({ e, theme, addUtilities }) => {
    addUtilities([...ratioUtilities({ e, theme })]);
  });

  it('should work with predefined values', async () => {
    // GIVEN
    const config = {
      content: [
        {
          raw: `
            <div class="twic-ratio-video" />
            <div class="twic-ratio-square" />
            <div class="twic-ratio-landscape" />
            <div class="twic-ratio-portrait" />
            `,
        },
      ],
    };

    // WHEN
    const result = await run(ratioUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });

  it('should work with custom values', async () => {
    // GIVEN
    const config: Config = {
      content: [
        {
          raw: `
            <div class="twic-ratio-custom" />
            `,
        },
      ],
      theme: {
        twicRatio: {
          custom: '1/2',
        },
      },
    };

    // WHEN
    const result = await run(ratioUtilitiesPlugin, config);

    // THEN
    expect(result).toMatchSnapshot();
  });
});
