import { CSSRuleObject, PluginAPI } from 'tailwindcss/types/config';

interface Options {
  e: PluginAPI['e'];
  theme: PluginAPI['theme'];
  defaults: Record<string | number, string>;
  twicConfigPath: string;
  cssClassPrefix: string;
  cssVariableName: string;
  valueTransformer?: (value: string) => string;
}

const utilitiesWithDefaults = ({
  theme,
  e,
  defaults,
  twicConfigPath,
  cssVariableName,
  cssClassPrefix,
  valueTransformer = (value) => value,
}: Options): CSSRuleObject[] => {
  const twicThemeConfig = theme(twicConfigPath, defaults);
  return Object.entries(twicThemeConfig).map(([key, value]) => ({
    [`.${e([cssClassPrefix, key].join('-'))}`]: {
      [cssVariableName]: valueTransformer(value),
    },
  }));
};

export { utilitiesWithDefaults };
