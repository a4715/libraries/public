import { CSSRuleObject, PluginAPI } from 'tailwindcss/types/config';

interface Options {
  e: PluginAPI['e'];
  theme: PluginAPI['theme'];
  tailwindConfigPath: string;
  twicConfigPath: string;
  cssClassPrefix: string;
  cssVariableName: string;
}

const utilitiesWithMergedPaths = ({
  theme,
  e,
  tailwindConfigPath,
  twicConfigPath,
  cssVariableName,
  cssClassPrefix,
}: Options): CSSRuleObject[] => {
  const defaultThemeConfig = theme(tailwindConfigPath);
  const twicThemeConfig = theme(twicConfigPath, {});
  return [
    ...Object.entries(defaultThemeConfig).filter(([key]) => key !== 'DEFAULT'),
    ...Object.entries(twicThemeConfig),
  ].map(([key, value]) => ({
    [`.${e([cssClassPrefix, key].join('-'))}`]: {
      [cssVariableName]: `${value}`,
    },
  }));
};

export { utilitiesWithMergedPaths };
