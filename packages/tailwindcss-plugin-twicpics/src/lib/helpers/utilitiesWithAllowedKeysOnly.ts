import { CSSRuleObject, PluginAPI } from 'tailwindcss/types/config';

interface Options {
  e: PluginAPI['e'];
  allowedKeys: string[];
  cssClassPrefix: string;
  cssVariableName: string;
}

const utilitiesWithAllowedKeysOnly = ({
  e,
  allowedKeys,
  cssClassPrefix,
  cssVariableName,
}: Options): CSSRuleObject[] =>
  allowedKeys.map((key) => ({
    [`.${e([cssClassPrefix, key].join('-'))}`]: {
      [cssVariableName]: key,
    },
  }));

export { utilitiesWithAllowedKeysOnly };
