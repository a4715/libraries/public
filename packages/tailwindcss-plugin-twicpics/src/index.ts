import plugin from 'tailwindcss/plugin';
import { modeUtilities } from './lib/utilities/modeUtilities';
import { positionUtilities } from './lib/utilities/positionUtilities';
import { ratioUtilities } from './lib/utilities/ratioUtilities';
import { transitionDelayUtilities } from './lib/utilities/transitionDelayUtilities';
import { transitionDurationUtilities } from './lib/utilities/transitionDurationUtilities';
import { transitionTimingFunctionUtilities } from './lib/utilities/transitionTimingFunctionUtilities';
import { zoomUtilities } from './lib/utilities/zoomUtilities';

export default plugin(({ e, addUtilities, theme }) => {
  addUtilities([
    ...modeUtilities({ e }),
    ...positionUtilities({ e }),
    ...ratioUtilities({
      e,
      theme,
    }),
    ...transitionDelayUtilities({
      e,
      theme,
    }),
    ...transitionTimingFunctionUtilities({
      e,
      theme,
    }),
    ...transitionDurationUtilities({
      e,
      theme,
    }),
    ...zoomUtilities({
      e,
      theme,
    }),
  ]);
});
